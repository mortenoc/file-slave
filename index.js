
exports.runFileSlave = function(scriptsPath, stylesPath, packagePath, project) {
    var fs = require("node-fs");
    var prompt = require("prompt");

    var promptSchema = {
        properties: {
            viewName: {
                description: "View Name (use / to create folders)",
                //pattern: /^[a-zA-Z0-9]+$/,
                message: "ViewName must be only letters or numbers",
                required: true
            },
            isPartial: {
                description: "Is this a partial view? (t/f)",
                type: "boolean",
                default: true
            },
            createModel: {
                description: "Create Model for view? (t/f)",
                type: "boolean",
                default: true
            },
            createStylus: {
                description: "Create stylus file? (t/f)",
                type: "boolean",
                default: true
            }
        }
    };

    var scriptsViewTemplate = "class {name} extends Backbone.View\n\n\tinitialize: ->\n\t\t";
    var scriptsModelTemplate = "class {name}Model extends Backbone.Model\n\n\tinitialize: ->\n\t\t";
    var stylusTemplate = "@import \"" + project.toLowerCase() + "/variables" + project.toLowerCase() + "\"\n\n";

    console.log(" ");
    console.log("MVC FileSlave will create coffee and stylus files based on input.");
    console.log("Remember to create styles/ and scripts/ root folders first!");
    console.log(" ");

    prompt.start();

    prompt.get(promptSchema, function (err, result) {

        var viewsDir = scriptsPath + "/Views";
        var viewPartialsDir = scriptsPath + "/Views/Partials";
        var modelsDir = scriptsPath + "/Models";
        var viewsStylDir = stylesPath + "/Views";
        var viewStylPartialsDir = stylesPath + "/Views/Partials";

        // Check if name has / (folder paths) in it
        if(result.viewName.indexOf("/") > -1)
        {
            var extraFolders = "/" + result.viewName.substring(0, result.viewName.lastIndexOf("/"));

            viewsDir = viewsDir + extraFolders;
            viewPartialsDir = viewPartialsDir + extraFolders;
            modelsDir = modelsDir + extraFolders;
            viewsStylDir = viewsStylDir + extraFolders;
            viewStylPartialsDir = viewStylPartialsDir + extraFolders;
        }

        // Create subfolders
        if(!result.isPartial){
            try {
                fs.accessSync(viewsDir, fs.F_OK);
            } catch (e) {
                fs.mkdirSync(viewsDir, 0777, true);
            }

            try {
                fs.accessSync(viewsStylDir, fs.F_OK);
            } catch (e) {
                fs.mkdirSync(viewsStylDir, 0777, true);
            }
        }
        
        if(result.isPartial){
            try {
                fs.accessSync(viewPartialsDir, fs.F_OK);
            } catch (e) {
                fs.mkdirSync(viewPartialsDir, 0777, true);
            }

            try {
                fs.accessSync(viewStylPartialsDir, fs.F_OK);
            } catch (e) {
                fs.mkdirSync(viewStylPartialsDir, 0777, true);
            }
        }

        if (result.createModel) {
            try {
                fs.accessSync(modelsDir, fs.F_OK);
            } catch (e) {
                fs.mkdirSync(modelsDir, 0777, true);
            }
        }

        // For package.json in the end:
        var viewToInject = "";
        var modelToInject = "";
        var stylToInject = "";
        var scriptsStaticFolder = "/scripts" + scriptsPath.split("scripts")[1];
        var viewName = result.viewName.substring(result.viewName.lastIndexOf("/")+1);

        console.log(" ");

        // Create View
        if (result.isPartial) {
            console.log("Creating Partial View: " + viewName + ".coffee");
            console.log("into: " + viewPartialsDir);

            var newViewPartialFile = viewName + ".coffee";
            fs.writeFileSync(viewPartialsDir + "/" + newViewPartialFile, scriptsViewTemplate.replace("{name}", viewName), "utf8", function (err) {
                if (err) {
                    return console.log(err);
                }
            });
            console.log(newViewPartialFile + " created successfully");
            viewToInject = scriptsStaticFolder + "/Views/Partials/" + result.viewName + ".js";
        }
        else {
            console.log("Creating Partial View: " + viewName);
            console.log("into: " + viewsDir);

            var newViewFile = viewName + ".coffee";
            fs.writeFileSync(viewsDir + "/" + newViewFile, scriptsViewTemplate.replace("{name}", viewName), "utf8", function (err) {
                if (err) {
                    return console.log(err);
                }
            });
            console.log(newViewFile + " created successfully");
            viewToInject = scriptsStaticFolder + "/Views/" + result.viewName + ".js";
        }

        // Create Model
        if (result.createModel) {
            console.log(" ");
            console.log("Creating Model: " + viewName + "Model.coffee");
            console.log("into: " + scriptsPath);

            var newModelFile = viewName + "Model.coffee";
            fs.writeFileSync(modelsDir + "/" + newModelFile, scriptsModelTemplate.replace("{name}", viewName), "utf8", function (err) {
                if (err) {
                    return console.log(err);
                }
            });
            console.log(newModelFile + " created successfully");
            modelToInject = scriptsStaticFolder + "/Models/" + result.viewName + "Model.js";
        }


        // Create Stylus
        if (result.createStylus) {
            console.log(" ");
            console.log("Creating stylus: " + viewName + ".styl");
            console.log("into: " + result.isPartial ? viewStylPartialsDir : viewsStylDir);

            var newStylFile = viewName + ".styl";
            fs.writeFileSync((result.isPartial ? viewStylPartialsDir : viewsStylDir) + "/" + newStylFile, stylusTemplate, "utf8", function (err) {
                if (err) {
                    return console.log(err);
                }
            });
            console.log(newStylFile + " created successfully");
            stylToInject = "/styles/" + result.viewName.replace(/\//g, '') + ".css";
        }


        // Update package.json files
        var pack = fs.readFileSync(packagePath);
        var content = JSON.parse(pack);
        var scriptsName = "scripts" + project;
        var stylesName = "styles" + project;

        if (result.createModel)
        {
            if(content.projectFiles[scriptsName].length > 0)
            {
                content.projectFiles[scriptsName].splice(content.projectFiles[scriptsName].length-1, 0, modelToInject);
                content.projectFiles[scriptsName].join();
            }
            else
            {
                content.projectFiles[scriptsName].push(modelToInject);
            }
        }

        // Inject script as second last because of Application.js file
        if(content.projectFiles[scriptsName].length > 0 || result.createModel)
        {
            content.projectFiles[scriptsName].splice(content.projectFiles[scriptsName].length-1, 0, viewToInject);
            content.projectFiles[scriptsName].join();
        }
        else
        {
            content.projectFiles[scriptsName].push(viewToInject);
        }

        if(stylToInject != "")
            content.projectFiles[stylesName].push(stylToInject);

        fs.writeFileSync(packagePath, JSON.stringify(content, null, 4));
    });
};