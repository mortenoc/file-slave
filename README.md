# FileSlave

FileSlave will create MVC structure files based on CLI inputs

## Features

* prompts the user for input
* creates files based on inputs
* updates package.json with new .css and .js files

## Install

1. add module to package.js

        "fileslave": "git+https://bitbucket.org/mortenoc/file-slave.git"
        

2. install module

        $ npm install


3. create new "fileslave.js" file in root of project and specify script and style src roots, and global package.json

        var fileslave = require("fileslave");
                
        fileslave.runFileSlave(
            "src/scripts",
            "src/styles",
            "./package.json");


## Usage

1. run FileSlave from root project dir
 
        $ node fileslave
        
2. follow instuctions

3. To create subfolders in folder, use / in name. Like Test/Menu
        